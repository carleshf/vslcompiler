using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using VSLCompiler.Compiler;

namespace VSLCompiler {
	
	/// <summary>
	/// Arguments: Class used to manage console-arguments.
	/// </summary>
	class Arguments {
		string[] raw_input;
		Dictionary<string, string> result;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="VSLCompiler.Arguments"/> class.
		/// </summary>
		/// <param name='p_input'>
		/// P_input.
		/// </param>
		public Arguments ( string[] p_input ) {
			this.raw_input = p_input;
			result = new Dictionary<string, string>();
			this.Process();
		}
		
		/// <summary>
		/// Process input arguments array to convert it to a dicionary.
		/// </summary>
		private void Process() {
			for ( int i = 0; i < this.raw_input.Length; i++ ) {
				if ( this.raw_input[ i ].StartsWith( "-" ) ) {
					if ( i + 1 < this.raw_input.Length && !this.raw_input[ i + 1 ].StartsWith( "-" ) ) {
						this.result.Add( this.raw_input[ i ], this.raw_input[ i + 1 ] );
					}
					else {
						this.result.Add( this.raw_input[ i ], null );
					}
				}
			}
		}
		
		/// <summary>
		/// Determines if our dictionary of arguments has the escified argument.
		/// </summary>
		/// <returns>
		/// <c>true</c> if our dictionary has the specified p_arg_name; otherwise, <c>false</c>.
		/// </returns>
		/// <param name='p_arg_name'>
		/// Name of the argument we're looking for.
		/// </param>
		public bool HasArg( string p_arg_name ) {
			try {
				string aux = this.result[ p_arg_name ];
				if ( aux == "" ) { }
				return true;
			}
			catch ( KeyNotFoundException ) {
				return false;
			}
		}
		
		/// <summary>
		/// Gets the argument if our dictionary of arguments has it.
		/// </summary>
		/// <returns>
		/// The argument content.
		/// </returns>
		/// <param name='p_arg_name'>
		/// Name of the argument we're looking for.
		/// </param>
		public string GetArg( string p_arg_name ) {
			try {
				return this.result[ p_arg_name ];
			}
			catch ( KeyNotFoundException ) {
				return null;
			}
		}
	}
	
	class MainClass {
		
		public static void Main ( string[] p_args ) {
			
			string input_file = "", output_file = "";
			
			Arguments args = new Arguments( p_args );
			
			if ( args.HasArg( "-h" ) ) {
				PrintHelp();
				return;
			}
			
			if ( args.HasArg( "-i" ) ) {
				input_file = args.GetArg( "-i" );
				if ( Path.GetExtension( input_file ) != ".vsl" ) {
					ShowParamError( "Wrong input file type detected." );
					return;
				}
			}
			else {
				ShowParamError( "No input file detected." );
				return;
			}
			
			if ( !args.HasArg( "-o" ) ) {
				ShowInfo( "No output file detected. Input file will be used." );
				output_file = input_file;
			}
			else {
				output_file = args.GetArg( "-o" );
			}
			
			bool monocecil = true;
			try{
				CodeGen.exceptIfNoMonoCecil();
			}
			catch ( Exception ) {
				monocecil = false;
			}
			if ( !monocecil ) {
				ShowParamError( "No \"Mono.Cecil\" or \"Mono.Cecil.Cil\" found. \"-c\" option will be disabled." );
			}
			
			try {
				// 1. Scanner
                Scanner scanner = null;
                using ( TextReader input = File.OpenText( input_file ) ) {
                    scanner = new Scanner(input);
					
					if ( args.HasArg( "-k" ) ) {
						System.Console.WriteLine( "" );
						ShowMessage( "Tokens:" );
						System.Console.WriteLine( "" );
						scanner.PrintTokens();
						System.Console.WriteLine( "\n" );
					}
                }
                
				// 2. Parser
				Parser parser = new Parser( scanner.Tokens );
				if ( args.HasArg( "-t" ) ) {
					System.Console.WriteLine( "" );
					ShowMessage("Tree:" );
					System.Console.WriteLine( "" );
					parser.PrintTree();
					System.Console.WriteLine( "\n" );
				}
				
				// 3. Code generation
				CodeGen code_gen = new CodeGen( parser.Result, Path.GetFileNameWithoutExtension( output_file ) + ".exe" );
				
				if ( args.HasArg( "-c" ) && monocecil ) {
					System.Console.WriteLine( "" );
					ShowMessage( "Code:" );
					System.Console.WriteLine( "" );
					code_gen.PrintInstr();
					System.Console.WriteLine( "" );
				}
				
				try {
					System.IO.File.Move( Path.GetFileNameWithoutExtension( output_file ) + ".exe", Path.Combine( Path.GetDirectoryName( output_file ), Path.GetFileNameWithoutExtension( output_file ) ) + ".exe" );
				}
				catch ( Exception ) { 
					System.IO.File.Delete( Path.GetFileNameWithoutExtension( output_file ) + ".exe" );
					ShowParamError( "Some error with output path/file has succeeded." );
				}
            }
            catch ( Exception e ) {
                Console.Error.WriteLine( e.Message );
            }
		}
		
		/// <summary>
		/// Shows the parameter as an error.
		/// </summary>
		/// <param name='p_message'>
		/// P_message.
		/// </param>
		private static void ShowParamError( string p_message ) {
			Console.BackgroundColor = ConsoleColor.White;
			System.Console.ForegroundColor = System.ConsoleColor.Red;
			System.Console.Write( "[ERROR]:");
			System.Console.ResetColor();
			System.Console.Write( "".PadRight( 8, ' ' ) );
			System.Console.WriteLine( p_message );
		}
		
		/// <summary>
		/// Shows the parameter as an info.
		/// </summary>
		/// <param name='p_message'>
		/// P_message.
		/// </param>
		private static void ShowInfo( string p_message ) {
			Console.BackgroundColor = ConsoleColor.White;
			System.Console.ForegroundColor = System.ConsoleColor.Blue;
			System.Console.Write( "[INFO] :" );
			System.Console.ResetColor();
			System.Console.Write( "".PadRight( 8, ' ' ) );
			System.Console.WriteLine( p_message );
		}
		
		/// <summary>
		/// Shows the parameter as a message.
		/// </summary>
		/// <param name='p_message'>
		/// P_message.
		/// </param>
		private static void ShowMessage( string p_message ) {
			Console.BackgroundColor = ConsoleColor.White;
			System.Console.ForegroundColor = System.ConsoleColor.Magenta;
			System.Console.Write( p_message );
			System.Console.ResetColor();
			System.Console.WriteLine( "" );
		}
		
		/// <summary>
		/// Prints the help.
		/// </summary>
		private static void PrintHelp() {
			
			System.Console.WriteLine( "\nVSLCompiler" );
			System.Console.WriteLine( "" );
			
			
			System.Console.WriteLine( "Usage:");
			System.Console.WriteLine( "   [ Win ]\n     VSLCompiler.exe [ -k | -t | -c ] -i input_file [ -o output_file ]" );
			System.Console.WriteLine( "   [ Nix ]\n     mono VSLCompiler.exe [ -k | -t | -c ] -i input_file [ -o output_file ]" );
			System.Console.WriteLine( "" );
			
			System.Console.WriteLine( "Options:" );
			System.Console.WriteLine( "   -h: [ optional ]   Shows help." );
			System.Console.WriteLine( "   -k: [ optional ]   Shows the tokens scanned by the compiler." );
			System.Console.WriteLine( "   -t: [ optional ]   Shows the sintax-tree created during the compilation." );
			System.Console.WriteLine( "   -c: [ optional ]   Shows the CIL code created during the compilation." );
			System.Console.WriteLine( "   -i: [ obligatory ] Path and name of the input source file." );
			System.Console.WriteLine( "   -o: [ optional ]   Path and name of the output object file." );
			System.Console.WriteLine( "                      If none, input source file's name will be used." );
			System.Console.WriteLine( "" );
		}
	}
}
