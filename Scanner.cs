using Collections = System.Collections.Generic;
using IO = System.IO;
using Text = System.Text;

namespace VSLCompiler.Compiler {
	/// <summary>
	/// Scanner.
	/// </summary>
	/// <exception cref='System.Exception'>
	/// Represents errors that occur during application execution.
	/// </exception>
	public class Scanner {
		
		/// <summary>
		/// Tokens: List of possible static tokens.
		/// </summary>
		public enum tokens { Addition, Substraction, Division, Production, 
			Assignment, Equating, Differentiation,
			LBracket, RBracket, Semicolon 
		};
		
		/// <summary>
		/// Var where we will store the result of the scanner process
		/// </summary>
		private readonly Collections.IList<object> result;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="VSLCompiler.Compiler.Scanner"/> class.
		/// </summary>
		/// <param name='p_input'>
		/// p_input is the input text strem to be scanned.
		/// </param>
		public Scanner ( IO.TextReader p_input ) {
			this.result = new Collections.List<object>();
			this.Scan( p_input );
		}
		
		/// <summary>
		/// Gets the tokens obtained after scannig the input text stream.
		/// </summary>
		/// <value>
		/// The tokens.
		/// </value>
		public Collections.IList<object> Tokens {
			get { return this.result; }
		}
		
		// -----------------------------------------------------------
		
		/// <summary>
		/// Scan the specified p_input.
		/// </summary>
		/// <param name='p_input'>
		/// p_input is the input text strem to be scanned.
		/// </param>
		/// <exception cref='System.Exception'>
		/// Represents errors that occur during application execution.
		/// </exception>
		private void Scan( IO.TextReader p_input ) {
			while ( p_input.Peek() != -1 ) {
				char read_ch = (char) p_input.Peek();
				
				if ( char.IsWhiteSpace( read_ch ) ) {
					// Skiping white spaces
					p_input.Read();
				}
				else if ( char.IsLetter( read_ch ) || read_ch == '_' ) {
					// Reading an identifier
					Text.StringBuilder accum = new Text.StringBuilder();
					while ( char.IsLetter( read_ch ) || read_ch == '_' ) {
						accum.Append( read_ch );
						p_input.Read();
						
						if ( p_input.Peek() == -1 ) {
							break;
						}
						
						read_ch = (char) p_input.Peek();
					}
					
					this.result.Add( accum.ToString() );
				}
				else if ( read_ch == '"' ) {
					// Reading a string literal
					Text.StringBuilder accum = new Text.StringBuilder();
					p_input.Read();	// Skiping "
					
					do {
						if ( p_input.Peek() == -1 ) {
							throw new System.Exception("Unterminated string literal.");
						}
						read_ch = (char) p_input.Peek();
						if ( read_ch != '"' ) {
							accum.Append( read_ch );
							p_input.Read();
						}
					} while ( read_ch != '"' );
					p_input.Read();
					this.result.Add( accum ); // NO .ToString(). We will use type TextBuffer in Parser.
				}
				else if ( char.IsDigit( read_ch ) ) {
					// Reading a numeric literal
					Text.StringBuilder accum = new Text.StringBuilder();
					bool is_float = false;
					while( char.IsDigit( read_ch ) || ( read_ch == '.' ) ) {
						accum.Append( read_ch );
						p_input.Read();
						
						if ( p_input.Peek() == -1 ) {
							break;
						}
						
						if ( read_ch == '.' ) {
							is_float = true;
						}
						
						read_ch = (char) p_input.Peek();
					}
					
					if ( is_float ) {
						this.result.Add( float.Parse( accum.ToString() ) );
					}
					else {
						this.result.Add( int.Parse( accum.ToString() ) );
					}
				}
				else if ( read_ch == '+' ) {
					p_input.Read();
					this.result.Add( Scanner.tokens.Addition );
				}
				else if ( read_ch == '-' ) {
					p_input.Read();
					this.result.Add( Scanner.tokens.Substraction );
				}
				else if ( read_ch == '*' ) {
					p_input.Read();
					this.result.Add( Scanner.tokens.Production );
				}
				else if ( read_ch == '/' ) {
					p_input.Read();
					this.result.Add( Scanner.tokens.Division );
				}
				else if ( read_ch == '=' ) {
					p_input.Read();
					if ( ( p_input.Peek() != -1 ) && ( p_input.Peek() == '=' ) ) {
						p_input.Read();
						this.result.Add( Scanner.tokens.Equating );
					}
					else {
						this.result.Add( Scanner.tokens.Assignment );
					}
				}
				else if ( read_ch == '<' ) {
					p_input.Read();
					if ( ( p_input.Peek() == -1 ) || ( p_input.Peek() != '>' ) ) {
						throw new System.Exception("Unterminated operation literal.");
					}
					else {
						p_input.Read();
						this.result.Add( Scanner.tokens.Differentiation );
					}
				}
				else if ( read_ch == ';' ) {
					p_input.Read();
					this.result.Add( Scanner.tokens.Semicolon );
				}
				else if ( read_ch == '(' ) {
					p_input.Read();
					if ( ( p_input.Peek() != -1 ) && ( p_input.Peek() == '*' ) ) {
						p_input.Read();
						bool end_comm = false;
						do {
							if ( p_input.Peek() == -1 ) {
								throw new System.Exception("Unterminated comment.");
							}
							read_ch = (char) p_input.Peek();
							p_input.Read();
							if ( ( read_ch == '*' ) && (p_input.Peek() == ')') ) {
								p_input.Read();
								end_comm = true;
							}
						} while ( !end_comm );
						p_input.Read();
					}
					else {
						this.result.Add( Scanner.tokens.LBracket );
						if ( p_input.Peek() == -1 ) {
							throw new System.Exception("Unterminated operation.");
						}
					}
				}
				else if ( read_ch == ')' ) {
					p_input.Read();
					this.result.Add( Scanner.tokens.RBracket );
				}
			}
		}
		
		// -----------------------------------------------------------
		
		/// <summary>
		/// Prints the tokens obtained from the input text-stream.
		/// </summary>
		public void PrintTokens() {
			int i = 1;
			foreach( object obj in this.Tokens ) {
				System.Console.Write( "{0,4}. ", i );
				System.Console.WriteLine( obj );
				i += 1;
			}
		}
	}
}

