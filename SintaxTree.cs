using System;

namespace VSLCompiler.Compiler.SintaxTree {
	
	//<instr_stmt> :=
	//	| var <identifier> = <expr_stmt>
	//	| <identifier> = <expr_stmt>
	//	| read_int <identifier>
	//	| read_str <identifier>
	//	| print <expr_stmt>
	//	| <instr_stmt>; <instr_stmt>
	//	| if <expr_stmt> then <instr_stmt> { else <instr_stmt> } endif
	//	| while <expr_stmt> do <instr_stmt> endwhile
	public abstract class InstrStmt { }
	
	// var <identifier> = <expr_stmt>
	public class VarStmt : InstrStmt {
		public string Identifier;
		public ExprStmt Expression;
	}
	
	// <identifier> = <expr_stmt>
	public class AssignStmt : InstrStmt {
		public string Identifier;
		public ExprStmt Expression;
	}
	
	// read_int <identifier>
	public class ReadIntStmt : InstrStmt {
		public string Identifier;
	}
	
	// read_str <identifier>
	public class ReadStrStmt : InstrStmt {
		public string Identifier;
	}
	
	// print <expr_stmt>
	public class PrintStmt : InstrStmt {
		public ExprStmt Expression;
	}
	
	// <instr_stmt>; <instr_stmt>
	public class Sequence : InstrStmt {
	    public InstrStmt First;
	    public InstrStmt Second;
	}
	
	// if <expr_stmt> then <instr_stmt> { else <instr_stmt> } endif
	public class IfStmt : InstrStmt {
		public ExprStmt Condition;
		public InstrStmt If_Body;
		public InstrStmt Else_Body;
	}
	
	// while <expr_stmt> do <instr_stmt> endwhile
	public class WhileStmt : InstrStmt { 
		public ExprStmt Condition;
		public InstrStmt While_Body;
	}
	
	// <expr_stmt> = <bool_stmt> { <bool_op> <bool_stmt> }
	public class ExprStmt {
		public BoolStmt Right;
		public BoolStmt Left;
		public BoolOp Op;
	}
	
	// <bool_op> := == | <>
	public enum BoolOp { Equating, Differentiation };
	
	// <bool_stmt> := <term_stmt> { <term_op> <term_stmt> }
	public class BoolStmt { 
		public TermStmt Right;
		public TermStmt Left;
		public TermOp Op;
	}
	
	// <term_op> := + | -
	public enum TermOp { Addition, Substraction };
	
	// <term_stmt> := <factor_stmt> { <factor_op> <factor_stmt> }
	public class TermStmt { 
		public FactorStmt Right;
		public FactorStmt Left;
		public FactorOp Op;
	}
	
	// <factor_op> := * | /
	public enum FactorOp { Division, Production };
	
	//<factor_stmt> := <identifier>
	//	| <number>
	//	| <string>
	//	| ( <expr_stm> )
	public abstract class FactorStmt { }
	
	// <string> := " <string_elem>* "
	public class StringLt : FactorStmt {
		public string Value;
	}
	
	// <int> := <digit>+
	public class IntLt : FactorStmt {
		public int Value;
	}
	
	public class FloatLt : FactorStmt {
		public float Value;
	}
	
	// <ident> := <char> <ident_rest>*
	// <ident_rest> := <char> | <digit>
	public class Variable : FactorStmt {
		public string Identifier;
	}
	
	// ( <expr_stm> )
	public class BracketExpr : FactorStmt {
		public ExprStmt Expression;
	}
}

