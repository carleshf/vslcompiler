using Collections = System.Collections.Generic;
using IO = System.IO;
using Text = System.Text;

using VSLCompiler.Compiler.SintaxTree;

namespace VSLCompiler.Compiler {
	public class Parser {
		
		private int index;
		private Collections.IList<object> tokens;
		private readonly InstrStmt result;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="VSLCompiler.Compiler.Parser"/> class.
		/// </summary>
		/// <param name='p_tokens'>
		/// P_tokens.
		/// </param>
		/// <exception cref='System.Exception'>
		/// Represents errors that occur during application execution.
		/// </exception>
		public Parser ( Collections.IList<object> p_tokens ) {
			this.tokens = p_tokens;
			this.index = 0;
			this.result = this.ParseInstr();
			
			if (this.index != this.tokens.Count) {
				throw new System.Exception("Expected EOF");
			}
		}
		
		/// <summary>
		/// Gets the result.
		/// </summary>
		/// <value>
		/// The result.
		/// </value>
		public InstrStmt Result {
			get { return result; }
		}
		
		// -----------------------------------------------------------
		
		private InstrStmt ParseInstr() {
			if ( this.index == this.tokens.Count) {
				throw new System.Exception("Expected statement, got EOF.");
			}
			
			
			InstrStmt result;
			
			if ( this.tokens[ this.index ].Equals( "if" ) ) {
				this.index++;
				IfStmt if_ = new IfStmt();
				if_.Condition = this.ParseExpr();
				if( !this.tokens[ this.index ].Equals( "then" ) ) {
					throw new System.Exception( "\"if\" statement without \"then\" keyword." );
				}
				this.index++;
				if_.If_Body = this.ParseInstr();
				
				if( this.tokens[ this.index ].Equals( "else" ) ) {
					this.index++;
					if_.Else_Body = this.ParseInstr();
				}
				else {
					if_.Else_Body = null;
				}
				
				
				if( !this.tokens[ this.index ].Equals( "endif" ) ) {
					throw new System.Exception( "\"if\" statement without \"endif\" keyword." );
				}
				
				this.index++;
				
				result = if_;
			}
			else if ( this.tokens[ this.index ].Equals( "print" ) ) {
				this.index++;
				PrintStmt print_ = new PrintStmt();
				print_.Expression = this.ParseExpr();
				result = print_;
			}
			else if ( this.tokens[ this.index ].Equals( "read_int" ) ) {
				this.index++;
				ReadIntStmt read_int = new ReadIntStmt();
				if ( this.index < this.tokens.Count && this.tokens[ this.index ] is string) {
					read_int.Identifier = (string) this.tokens[ this.index++ ];
					result = read_int;
				}
				else {
					throw new System.Exception("Expected variable name after \"read_int\"");
				}
			}
			else if( this.tokens[ this.index ].Equals( "read_str" ) ) {
				this.index++;
				ReadStrStmt read_str = new ReadStrStmt();
				if ( this.index < this.tokens.Count && this.tokens[ this.index ] is string ) {
					read_str.Identifier = (string) this.tokens[ this.index++ ];
					result = read_str;
				}
				else {
					throw new System.Exception( "Expected variable name after \"read_str\"" );
				}
			}
			else if( this.tokens[ this.index ].Equals( "var" ) ) {
				this.index++;
				if ( this.index < this.tokens.Count && this.tokens[ this.index ] is string ) {
					VarStmt var_ = new VarStmt();
					var_.Identifier = (string) this.tokens[ this.index++ ];
					
					
					if ( this.index == this.tokens.Count || !this.tokens[ this.index ].Equals( Scanner.tokens.Assignment ) ) {
						throw new System.Exception( "Expected \"=\" after \"var identifier\"" );
					}
					this.index++;
					var_.Expression = this.ParseExpr();
					
					result = var_;
				}
				else {
					throw new System.Exception("Expected variable name after \"var\"");
				}
			}
			else if ( this.tokens[ this.index].Equals( "while" ) ) {
				this.index++;
				WhileStmt while_ = new WhileStmt();
				while_.Condition = this.ParseExpr();
				
				if( !this.tokens[ this.index ].Equals( "do" ) ) {
					throw new System.Exception( "\"while\" statement without \"do\" keyword." );
				}
				this.index++;
				while_.While_Body = this.ParseInstr();
				
				if( !this.tokens[ this.index ].Equals( "endwhile" ) ) {
					throw new System.Exception( "\"while\" statement without \"endwhile\" keyword." );
				}
				
				this.index++;
				
				result = while_;
				
			}
			else if ( this.tokens[ this.index ] is string ) { // Assignment
				AssignStmt assign = new AssignStmt();
				assign.Identifier = (string) this.tokens[ this.index++ ];
				if ( this.index == this.tokens.Count || !this.tokens[ this.index ].Equals( Scanner.tokens.Assignment ) ) {
					throw new System.Exception( "Expected \"assignment\" after token " + this.tokens[ this.index-1 ].ToString() );
				}
				
				this.index++;
				assign.Expression = this.ParseExpr();
				
				result = assign;
			}
			else {
				throw new System.Exception( "Parse error at token " + this.index + ": " + this.tokens[ this.index ] );
			}
			
			
			
			if ( this.index < this.tokens.Count && this.tokens[ this.index ].Equals( Scanner.tokens.Semicolon ) ) {
				
				this.index++;
	
				if ( this.index < this.tokens.Count && !this.tokens[ this.index ].Equals( "else" ) &&
						!this.tokens[ this.index ].Equals( "endif" ) && !this.tokens[ this.index ].Equals( "endwhile" ) ) {
					Sequence sqc = new Sequence();
					sqc.First = result;
					sqc.Second = this.ParseInstr();
					result = sqc;
				}
			}
			else if ( !this.tokens[ this.index ].Equals( Scanner.tokens.Semicolon ) ) {
				throw new System.Exception("Expected \"semicolon\" after token " + this.tokens[ this.index-1 ].ToString());
			}
			
			
			return result;
		}
		
		/// <summary>
		/// This function is used to parses a expression statement.
		/// </summary>
		/// <returns>
		/// The expr.
		/// </returns>
		/// <exception cref='System.Exception'>
		/// Represents errors that occur during application execution.
		/// </exception>
		private ExprStmt ParseExpr() {
			if( this.index == this.tokens.Count) {
				throw new System.Exception( "Expected statement, got EOF." );
			}
			
			
			ExprStmt result = new ExprStmt();
			
			result.Left = this.ParseBool();
			
			if( this.index < this.tokens.Count) {
				bool next = true;
				
				if( this.tokens[ this.index ].Equals( Scanner.tokens.Equating ) ) { result.Op = BoolOp.Equating; }
				else if( this.tokens[ this.index ].Equals( Scanner.tokens.Differentiation ) ) { result.Op = BoolOp.Differentiation; }
				else{ 
					next = false; 
					result.Right = null;
				}
				
				if( next ){ 
					this.index++;
					result.Right = this.ParseBool(); 
				}
			}
			else { 
				result.Right = null;
			}
			
			return result;
		}
		
		/// <summary>
		/// This function is used to parses the bool statement.
		/// </summary>
		/// <returns>
		/// The bool.
		/// </returns>
		/// <exception cref='System.Exception'>
		/// Represents errors that occur during application execution.
		/// </exception>
		private BoolStmt ParseBool() {
			if( this.index == this.tokens.Count ) {
				throw new System.Exception( "Expected statement, got EOF." );
			}
			
			
			BoolStmt result = new BoolStmt();
			
			result.Left = this.ParseTerm();
			
			if( ( this.index < this.tokens.Count ) && 
				( this.tokens[ this.index ].Equals( Scanner.tokens.Addition ) || this.tokens[ this.index ].Equals( Scanner.tokens.Substraction ) ) ) {
				if( this.tokens[ this.index ].Equals( Scanner.tokens.Addition ) ) { result.Op = TermOp.Addition; }
				else{ result.Op = TermOp.Substraction; }
				
				this.index++;
				result.Right = this.ParseTerm();
			}
			else { 
				result.Right = null;
			}
			
			return result;
		}
		
		/// <summary>
		/// This function is used to parses a term statement.
		/// </summary>
		/// <returns>
		/// The term.
		/// </returns>
		/// <exception cref='System.Exception'>
		/// Represents errors that occur during application execution.
		/// </exception>
		private TermStmt ParseTerm() {
			if( this.index == this.tokens.Count ) {
				throw new System.Exception( "Expected statement, got EOF." );
			}
			
			
			TermStmt result = new TermStmt();
			
			result.Left = this.ParseFctr();
			
			if( ( this.index < this.tokens.Count ) && 
				( this.tokens[ this.index ].Equals( Scanner.tokens.Production ) || this.tokens[ this.index ].Equals( Scanner.tokens.Division ) ) ) {
				if( this.tokens[ this.index ].Equals( Scanner.tokens.Production ) ) { result.Op = FactorOp.Production; }
				else { result.Op = FactorOp.Division; }
				
				this.index++;
				result.Right = this.ParseFctr();
			}
			else { 
				result.Right = null;
			}
			
			return result;
		}
		
		/// <summary>
		/// This functions is used to parse a factor statement.
		/// </summary>
		/// <returns>
		/// The factor.
		/// </returns>
		/// <exception cref='System.Exception'>
		/// Represents errors that occur during application execution.
		/// </exception>
		private FactorStmt ParseFctr() {
			if( this.index == this.tokens.Count ) {
				throw new System.Exception( "Expected statement, got EOF." );
			}
			
			if( this.tokens[ this.index ] is Text.StringBuilder ) {
				string strValue = ( (Text.StringBuilder) this.tokens[ this.index++ ] ).ToString();
				StringLt stringLiteral = new StringLt();
				stringLiteral.Value = strValue;
				return stringLiteral;
			}
			else if( this.tokens[ this.index ] is int ) {
				int intValue = (int) this.tokens[ this.index++ ];
				IntLt intLiteral = new IntLt();
				intLiteral.Value = intValue;
				return intLiteral;
			}
			else if( this.tokens[ this.index ] is float ) {
				float floatValue = (float) this.tokens[ this.index++ ];
				FloatLt floatLiteral = new FloatLt();
				floatLiteral.Value = floatValue;
				return floatLiteral;
			}
			else if( this.tokens[ this.index ] is string ) {
				string identifier = (string) this.tokens[ this.index++ ];
				Variable variable = new Variable();
				variable.Identifier = identifier;
				return variable;
			}
			else if( this.tokens[ this.index ].Equals( Scanner.tokens.LBracket ) ) {
				this.index++; // LBracket
				BracketExpr expr = new BracketExpr();
				expr.Expression = this.ParseExpr();
				if ( this.tokens[ this.index ].Equals( Scanner.tokens.RBracket ) ) {
					this.index++; // RBracket
					return expr;
				}
				else {
					throw new System.Exception( "Expected \"right bracket\" after \"left bracker\" and \"expression\"." );
				}
			}
			else {
				throw new System.Exception( "Expected \"string literal\", \"int literal\", \"float literal\", or variable." );
			}
		}
		
		
		
		
		// -----------------------------------------------------------
		
		/// <summary>
		/// Prints the tree generated using the input tokens list.
		/// </summary>
		public void PrintTree() {
			PrintTree( this.result, 0 );
		}
		
		/// <summary>
		/// Prints the tree generated using the input tokens list.
		/// </summary>
		/// <param name='p_item'>
		/// p_item to print.
		/// </param>
		/// <param name='p_level'>
		/// p_level to start printing.
		/// </param>
		private void PrintTree(object p_item, int p_level) {
			if ( p_item is string ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( p_item );
			}
			else if ( p_item is FloatLt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<FloatLt> " + ( (FloatLt) p_item ).Value );
			}
			else if ( p_item is IntLt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<IntLt> " + ( (IntLt) p_item ).Value );
			}
			else if (p_item is StringLt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<StringLt> " + ( (StringLt) p_item ).Value );
			}
			else if ( p_item is Variable ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<Variable> " + ( (Variable) p_item ).Identifier );
			}
			else if ( p_item is TermStmt ) {
				this.PrintLvl( p_level );
				if ( ( (TermStmt) p_item ).Right == null ) {
					System.Console.WriteLine( "<Term>" );
					this.PrintTree( ( (TermStmt) p_item ).Left, p_level + 1 );
				}
				else {
					System.Console.WriteLine( "<Term> " + ( (TermStmt)p_item ).Op );
					this.PrintTree( ( (TermStmt) p_item ).Left, p_level + 1 );
					this.PrintTree( ( (TermStmt) p_item ).Right, p_level + 1 );
				}
			}
			else if ( p_item is BoolStmt ) {
				this.PrintLvl( p_level );
				if ( ( (BoolStmt) p_item ).Right == null ) {
					System.Console.WriteLine( "<Bool>" );
					this.PrintTree( ( (BoolStmt) p_item ).Left, p_level + 1 );
				}
				else {
					System.Console.WriteLine( "<Bool> " + ( (BoolStmt) p_item ).Op );
					this.PrintTree( ( (BoolStmt) p_item ).Left, p_level + 1 );
					this.PrintTree( ( (BoolStmt) p_item ).Right, p_level + 1 );
				}
			}
			else if ( p_item is ExprStmt ) {
				this.PrintLvl( p_level );
				if ( ( (ExprStmt) p_item ).Right == null ) {
					System.Console.WriteLine( "<Expr>" );
					this.PrintTree( ( (ExprStmt) p_item ).Left, p_level + 1 );
				}
				else {
					System.Console.WriteLine( "<Expr> " + ( (ExprStmt) p_item).Op );
					this.PrintTree( ( (ExprStmt) p_item ).Left, p_level + 1 );
					this.PrintTree( ( (ExprStmt) p_item ).Right, p_level + 1 );
				}
			}
			else if ( p_item is IfStmt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<IfStm>" );
				this.PrintTree( ( (IfStmt) p_item ).Condition, p_level + 1 );
				this.PrintTree( ( (IfStmt) p_item ).If_Body, p_level + 1 );
				if ( ( (IfStmt) p_item ).Else_Body != null ) {
					this.PrintTree( ( (IfStmt) p_item ).Else_Body, p_level + 1 );
				}				
			}
			else if ( p_item is PrintStmt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<PrintStm>" );
				this.PrintTree( ( (PrintStmt) p_item ).Expression, p_level + 1 );
			}
			else if ( p_item is ReadIntStmt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<ReadIntStm>" );
				this.PrintTree( ( (ReadIntStmt) p_item ).Identifier, p_level + 1 );
			}
			else if ( p_item is ReadStrStmt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<ReadStrStm>" );
				this.PrintTree( ( (ReadStrStmt) p_item ).Identifier, p_level + 1 );
			}
			else if ( p_item is VarStmt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<VarStm>" );
				this.PrintTree( ( (VarStmt) p_item ).Identifier, p_level + 1 );
				this.PrintTree( ( (VarStmt) p_item ).Expression, p_level + 1 );
			}
			else if ( p_item is WhileStmt ) {
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<WhileStm>" );
				this.PrintTree( ( (WhileStmt) p_item ).Condition, p_level + 1 );
				this.PrintTree( ( (WhileStmt) p_item ).While_Body, p_level + 1 );
			}
			else if( p_item is Sequence ) {
				this.PrintTree( ( (Sequence) p_item ).First, p_level );
				this.PrintTree( ( (Sequence) p_item ).Second, p_level );
			}
			else if ( p_item is BracketExpr ) {
				this.PrintTree( ( (BracketExpr) p_item ).Expression, p_level );
			}
			else if ( p_item is AssignStmt ) { 
				this.PrintLvl( p_level );
				System.Console.WriteLine( "<AssignStmt>" );
				this.PrintTree( ( (AssignStmt) p_item ).Identifier, p_level + 1 );
				this.PrintTree( ( (AssignStmt) p_item ).Expression, p_level + 1 );
			}
		}
		
		/// <summary>
		/// Axuxiliar function used in printing tree.
		/// </summary>
		/// <param name='p_level'>
		/// P_level.
		/// </param>
		private void PrintLvl( int p_level ) {
			for(int i = 0; i < p_level * 2; i++) { System.Console.Write( " " ); }
			System.Console.Write( "+-" );
		}
		
		// -----------------------------------------------------------
	}
}

